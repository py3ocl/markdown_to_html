# markdown_to_html

![](header_image.jpg)

## General information
Convert markdown (.md) files to html files.

For information about valid Markdown syntax that is interpreted see:  
[Markdown Guide](https://www.markdownguide.org/)  

The converter offers built-in default CSS styles to provide a reasonable layout, which can be overridden using --import-css or --include-css.

When providing your own CSS file you will need to add the following for lines ending with two spaces:
```
.multiline {
  white-space: pre-wrap;
}
```
It is possible to add your own multi-line CSS style,  
but you will need to test the output for multiple lines of text.

## Syntax
```
Markdown2Html.py <input_file.md> or <input directory>
                 [output_file] or [output directory]
                 [--import-css=my_style.css]
                 [--include-css=my_style.css]
                 [--export-css=my_style.css]
                 [--title="Title of page"]
                 [--recursive]
                 [--verbose]
```

## Description of options
```
Markdown2Html.py

    --import-css    ...   Read a .css file and embed it's contents in the generated .html file.
    --include-css   ...   Add <link rel="stylesheet" href=""> to include the css file.
    --export-css    ...   Export the default CSS styles to a css file,
                          allowing the generation of a default template.
    --title               Text used for the title of the page.
    --borders       ...   Add borders around all html elements to help debug output.
```

## Examples of using the tool
Examples of creating .html file with same name as .md file (.md replaced with .html):
```
Markdown2Html.py my_doc.md
```

Example of creating .html file with different name:
```
Markdown2Html.py my_doc.md my_doc_page.html
```

Example of creating .html file and importing CSS file:
```
Markdown2Html.py my_doc.md --import-css=style.css
```

Example of creating .html file and including CSS file:
```
Markdown2Html.py my_doc.md --include-css=style.css
```

Example of creating .html files recursively from a source directory containing markdown files.
```
Markdown2Html.py markdown_folder html_folder --recursive
```
