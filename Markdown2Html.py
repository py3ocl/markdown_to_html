#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import platform
from internal.Markdown2HtmlParsers import Markdown2HtmlParsers
from internal.FileUtility import FileUtility
from internal.HtmlUtility import HtmlUtility

class Markdown2Html:

    ## Read a text file which stores the Markdown text, and generate the HTML lines.
    def __init__(self):

        self._eol = "\r\n" if platform.system() == "Windows" else "\n"
        self._argv = []
        self._parsers = Markdown2HtmlParsers()

    ## Parse an argument starting with "--"
    def _ParseSwitch(self, switch):
        assert switch.startswith("--")

        # Either set the value after = or set to True.
        if switch.startswith("--"):
            switch = switch[2:]
            pos = switch.find("=")
            value = switch[pos + 1:] if pos != -1 else True
            if pos != -1:
                switch = switch[0:pos]

            #  Switch should always exist with default value.
            if self._parsers.GetSwitch(switch) is None:
                return False

            self._parsers.SetSwitch(switch, value)
            return True

        return False

    ## Parse system arguments, which will be come from sys.argv.
    ## Return None or string containing parsing error message.
    def _ParseSystemArgs(self):
        error = None

        infile = ""
        outfile = ""

        for arg in self._argv:

            # Handle all switches with or without =, e.g. --css-import=styles.css
            if arg.startswith("--"):
                if arg == "--help":
                    self.PrintHelp()
                    sys.exit(0)

                if not self._ParseSwitch(arg):
                    return "Unexpected argument {}\n".format(arg)

            # When not a switch (starting with --) then its an input or output filename.
            elif not infile:
                infile = arg
            elif not outfile:
                outfile = arg

        recursive = self._parsers.GetSwitch(Markdown2HtmlParsers.KEY_RECURSIVE)

        if recursive:
            if infile and outfile:
                self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_INPUT_DIR, infile)
                self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_DIR, outfile)
            else:
                return "Must input and output directories"
        elif infile:
            if not outfile:
                pos = infile.rfind(".")
                outfile = infile[:pos] + ".html" if (pos != -1) else infile + ".html"
            self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_INPUT_FILE, infile)
            self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_FILE, outfile)
        else:
            return "Must provide input filename or --recursive"

        # No error
        return None

    ## Convert the Markdown file to HTML file.
    def ConvertMarkdownToHtml(self, argv):
        self._argv = argv
        error = self._ParseSystemArgs()

        if not error:
            css_lines = []
            css_import = self._parsers.GetSwitch(Markdown2HtmlParsers.KEY_CSS_IMPORT)
            css_include = self._parsers.GetSwitch(Markdown2HtmlParsers.KEY_CSS_INCLUDE)

            # Set CSS styles to user defined import when specified.
            if not css_include:
                if css_import:
                    css_import = "default.css"
                    (css_lines, error) = FileUtility.Read(css_import)
                    if not error:
                        self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_CSS_LINES, css_lines)
                else:
                    # When user specified CSS styles import not used
                    (css_lines, error) = FileUtility.Read("default.css")
                    if error:
                        css_lines = HtmlUtility.GetDefaultCssStyles()
                    self._parsers.SetAttribute(Markdown2HtmlParsers.KEY_CSS_LINES, css_lines)
                    error = None

        if error:
            return error

        return self._parsers.ConvertMarkdownToHtml()

    ## Get any error caused by reading the file or generating HTML lines.
    def GetError(self):
        return self._error

    ## Print the help for the markdown to html tool.
    def PrintHelp(self):
        switch_padding = "              "
        help = "Release date - 17/01/2021\n\n" \
               "Markdown2Html <markdown filename or input directory> " \
               "<html filename or output directory>\n"

        switches = self._parsers.GetSwitches()
        longest_switch = len(max(switches, key=len))

        desc_padding = " " * (longest_switch - 4)
        help += "{}--help{}   {}\n".format(switch_padding, desc_padding, "Show this help")

        for switch in switches:
            switch_desc = self._parsers.GetSwitchDescription(switch)
            desc_padding = " " * (longest_switch - len(switch))
            help += "{}--{}{}   {}\n".format(switch_padding, switch, desc_padding, switch_desc)

        help += "\nExamples:\n\n"
        help += "    Markdown2Html.py my_doc.md --{}=style.css\n".format(Markdown2HtmlParsers.KEY_CSS_INCLUDE)
        help += "    Markdown2Html.py my_doc.md my_doc.html --{}=style.css\n".format(Markdown2HtmlParsers.KEY_CSS_IMPORT)
        help += "    Markdown2Html.py markdown_dir html_dir --{}".format(Markdown2HtmlParsers.KEY_RECURSIVE)
        print(help)

if __name__ == "__main__":

    # NOTE: eol not supported at the moment for linux/windows line endings.
    md2html = Markdown2Html()
    error = md2html.ConvertMarkdownToHtml(sys.argv[1:])
    if error:
        sys.stderr.write(error + "\n")
