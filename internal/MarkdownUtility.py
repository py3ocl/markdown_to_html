#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .StringUtility import StringUtility

class MarkdownUtility:

    ## Return True when a Markdown line ends with "  ", indicating a forced new line.
    @staticmethod
    def IsForcedNewLine(line):
        return line.endswith("  ")

    ## Check the start of the Markdown is an unordered list or
    ## checkbox syntax starting with "- [X]", "- [ ]" or "- []"
    @staticmethod
    def IsUnorderedListOrCheckbox(text, checkbox = False):
        text = text.strip()
        if text and text[0] == '-':
            if checkbox:
                text = text[1:].strip()
                ret_val = text.startswith("[X]") or text.startswith("[ ]") or text.startswith("[]")
            else:
                text = text[1:].strip()
                ret_val = not text.startswith("[X]") and \
                          not text.startswith("[ ]") and \
                          not text.startswith("[]")
        else:
            ret_val = False
        return ret_val

    ## Get the line for a ordered or unordered list, split into indent, number and text
    ## Return is (indent count, number or None, text) or (0, None, "") if invalid.
    @staticmethod
    def SplitListLine(line, numeric = False, checkbox = False):
        indent = StringUtility.CountStartChars(line, ' ')
        line = line.strip()
        checked = False

        if numeric:
            number = StringUtility.GetNumericString(line, ".")
            number_len = len(number)
            text = line[number_len + 1:] if number and line[number_len] == "." else ""
        else:
            number = None
            if checkbox:
                text = line[1:].strip() if len(line) and line[0] == "-" else ""
                if text.startswith("[X]"):
                    checked = True
                    text = text[3:]
                elif text.startswith("[]"):
                    text = text[2:]
                elif text.startswith("[ ]"):
                    text = text[3:]
                else:
                    # Invalid checkbox syntax.
                    text = ""
            else:
                text = line[1:].strip() if len(line) and line[0] == "-" else ""

        return (indent, number, text.strip(), checked)
