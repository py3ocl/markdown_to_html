#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import base64
from .StringUtility import StringUtility

class FileUtility:

    ## Read a file as binary and return the data as a binary decoded string.
    @staticmethod
    def Read(filename):
        error = None
        data = None

        try:
            with open(filename, "rb") as f:
                data = f.read()
            data = data.decode()
        except OSError as oe:
            data = None
            error = "error code {} while opening file {}".format(oe.errno, filename).strip()
        except UnicodeDecodeError as ue:
            data = None
            error = "{} (encoding {}) for file {}".format(ue.reason, ue.encoding, filename).strip()
        except:
            data = None
            error = "Unknown read error for file {}".format(filename)

        lines = StringUtility.ConvertBinaryDataToLines(data) if data else []
        return (lines, error)

    ## Write the lines as a text file.
    @staticmethod
    def Write(filename, lines, eol = None, binary = False):
        error = None
        if not binary or not eol:
            eol = "\n"

        try:
            with open(filename, "w") as f:
                for line in lines:
                    f.write(line + eol)
        except OSError as oe:
            error = "error code {} for file {}".format(oe.errno, filename).strip()
        except:
            error = "Unknown write error for file {}".format(filename)

        return error

    ## Open an image and return as a base-64 buffer.
    @staticmethod
    def GetBase64Image(filename):
        image = open(filename)
        image_data = image.read()
        encoded_image_data = base64.b64encode(image_data)
        image_data_utf8 = encoded_image_data.decode('utf-8')
        return image_data_utf8

    ## Match the filename extension to one provided in extensions.
    ## extensions can be a list of a string of extensions separated by a character.
    @staticmethod
    def MatchExtension(filename, extensions, sep = ';'):
        if extensions:
            if type(extensions) is str:
                extensions = extensions.split(sep)
            for e in extensions:
                if filename.endswith(e):
                    return True
            return False
        return True

    ## Create all the folders of the full path and return True on success.
    @staticmethod
    def CreateDirectory(path, exists_as_error = False):
        try:
            os.makedirs(path)
        except FileExistsError:
            return False if exists_as_error else True
        except:
            return False
        return True

    ## Find files and folders, which can optionally be done recursively.
    @staticmethod
    def FindFiles(matched_files, matched_paths, invalid_paths, extensions, path, recursive = True):

        assert(type(matched_files) is list)
        assert(matched_paths is None or type(matched_paths) is list)
        assert(invalid_paths is None or type(invalid_paths) is list)

        try:
            files = os.listdir(path)
        except:
            if invalid_paths is not None:
                invalid_paths.append(path)
        else:
            for f in files:
                file_path = os.path.join(path, f) if path and path != "." and path != ".." else f
                if os.path.isdir(file_path):
                    if recursive and f != '..' and f != '.':
                        if matched_paths is not None:
                            matched_paths.append(file_path)
                        if not FileUtility.FindFiles(matched_files, matched_paths, invalid_paths, extensions, file_path, True):
                            return False
                elif FileUtility.MatchExtension(file_path, extensions):
                    matched_files.append(file_path)
        return True

    ## Convert a filename to a new file extension.
    @staticmethod
    def ReplaceFilenameExtension(filename, old_extension, new_extension):
        if filename.endswith(old_extension):
            return filename[:-len(old_extension)] + new_extension
        return filename

    ## When a path exists with a filename, replace it with the new path.
    @staticmethod
    def ReplacePath(filename, old_path, new_path):
        pos = filename.find(old_path)
        if pos != -1:
            filename = filename[:pos] + new_path + filename[pos + len(old_path):]
        return filename
