#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser

class Markdown2HtmlBlockquoteParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse markdown blockquote lines, where lines start with ">".
    def Parse(self, text_rows):
        html_lines = []
        row_pos = text_rows.GetRowPosition()
        row = text_rows.GetRowAt(row_pos)

        while row.startswith(">"):
            row_pos += 1
            html_lines.append(row.replace(">", "&gt;"))
            row = text_rows.GetRowAt(row_pos)

        if html_lines:
            html_lines[0] = "<blockquote>" + html_lines[0]
            html_lines[-1] = html_lines[-1] + "</blockquote>"
            text_rows.IncrementRowPosition(len(html_lines))

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlBlockquoteParser"
