#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser

## Convert markdown horizontal line, where following line starts has blank line,
## then is "---", and another blank line.
class Markdown2HtmlLineParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next 2 markdown lines,
    ## where the second row is "---" between two blank lines.
    def Parse(self, text_rows):
        html_lines = []
        rows = text_rows.GetRows(True, 3)
        if len(rows) == 3 and not rows[0].strip() and not rows[2].strip():
            row = rows[1].strip()
            invalid = False
            for c in row:
                if c != '-':
                    invalid = True
                    break

            # If the syntax is only dash characters, then create the HTML row.
            if row and not invalid:
                html_lines = ["<hr>"]
                text_rows.IncrementRowPosition(3)

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlLineParser"
