#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlCharacterMap import HtmlCharacterMap

class Markdown2HtmlEscapeParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)
        self._html_character_map = HtmlCharacterMap()

    ## Parse the next character, and identify escaped characters for conversion to HTML.
    def Parse(self, text_rows):
        html_lines = []
        row = text_rows.GetRow(True)

        if row.startswith("``") or \
           (row.startswith('\\') and len(row) > 1):
            html_char = self._html_character_map.Lookup(row[1])
            if html_char:
                text_rows.IncrementColumnPosition(2)
                html_lines = [html_char]

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlEscapeParser"
