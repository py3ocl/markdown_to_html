#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlUtility import HtmlUtility
from ..StringUtility import StringUtility

## Convert markdown headers starting with '#'
class Markdown2HtmlHeadersParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the current markdown row, which will be a header starting with '#'.
    def Parse(self, text_rows):
        html_lines = []
        row = text_rows.GetRow()

        if row.startswith("#"):
            count = StringUtility.CountStartChars(row, '#')
            tag = "h" + str(count)
            tagged_text = HtmlUtility.GetTaggedString(tag, row[count:].strip())
            html_lines = [tagged_text]
            text_rows.IncrementRowPosition()

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlHeadersParser"
