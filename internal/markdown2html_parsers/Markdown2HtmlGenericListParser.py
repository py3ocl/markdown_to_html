#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlUtility import HtmlUtility
from ..StringUtility import StringUtility
from ..MarkdownUtility import MarkdownUtility

class Markdown2HtmlGenericListParser(Parser):

    ## Perform any initialisation from the base class.
    def __init__(self):
        Parser.__init__(self)

    ## Parse an ordered or unordered list, with or without checkbox.
    @staticmethod
    def ParseList(text_rows, numeric = False, checkbox = False):
        indent_diff = 0
        prev_indent = 0
        parse_error = False
        indent_tag = "ol" if numeric else "ul"
        extra_indent_tag = " style=\"list-style-type:none;\"" if checkbox else ""
        row_pos = text_rows.GetRowPosition()
        start_row = row_pos
        html_lines = ["<{}{}>".format(indent_tag, extra_indent_tag)]
        error_string = ""

        # The ordered list number for the level of indentation.
        expected_indent_number = [0]

        row = text_rows.GetRowAt(row_pos)

        # Make sure this is the correct parser for numeric or non-numeric before continuing.
        parser_matches = StringUtility.IsNumericString(row, '.') if numeric else \
                         MarkdownUtility.IsUnorderedListOrCheckbox(row, checkbox)

        if not parser_matches:
            return ([], "Lists cannot be parsed")

        (indent, number_str, row, checked) = MarkdownUtility.SplitListLine(row, numeric, checkbox)

        while not parse_error and row:
            row_pos += 1

            # Handle indent and expected spaces for each indent.
            if indent_diff == 0 and prev_indent != indent:
                indent_diff = abs(indent - prev_indent)

            # Handle missing row and invalid indentation first.
            if not row:
                parse_error = True
                error_string = "Text missing on row_pos {}".format(row_pos)
            elif abs(indent - prev_indent) != 0 and abs(indent - prev_indent) != indent_diff:
                parse_error = True
                error_string = "Indent error on row_pos {}".format(row_pos)
            else:
                # Handle expected numbers for indent, un-indent and no indent.
                if indent > prev_indent:
                    expected_indent_number.append(1)
                    html_lines.append("<{}{}>".format(indent_tag, extra_indent_tag))
                elif indent < prev_indent:
                    expected_indent_number.pop()
                    expected_indent_number[-1] = expected_indent_number[-1] + 1
                    html_lines.append("</{}>".format(indent_tag))
                else:
                    expected_indent_number[-1] = expected_indent_number[-1] + 1

                # Verify the number matches the expected number in sequence, supporting step of only 1.
                if number_str and int(number_str) != expected_indent_number[-1]:
                    parse_error = True
                    error_string = "Invalid number on row_pos {}, expecting {} and got {}".format(
                                         row_pos, expected_indent_number[-1], number_str)
                else:
                    # No errors, so add the ordered number element.
                    if checkbox:
                        checked_str = " checked=\"checked\"" if checked else ""
                        input_str = "<input type=\"checkbox\"{} onclick=\"return false;\"/>".format(checked_str)
                        html_text = "<div class=\"multiline\"><label>{}{}</label></div>".format(input_str, row)
                        html_lines.append(html_text)
                    else:
                        tagged_line = HtmlUtility.GetTaggedString("li", row)
                        html_lines.append(tagged_line)
                    row = text_rows.GetRowAt(row_pos)
                    prev_indent = indent
                    (indent, number_str, row, checked) = MarkdownUtility.SplitListLine(row, numeric, checkbox)

        if not parse_error and html_lines:
            while indent_diff and indent > 0:
                html_lines.append("</{}>".format(indent_tag))
                indent -= indent_diff
            html_lines.append("</{}>".format(indent_tag))
            num_rows = row_pos - start_row
            text_rows.IncrementRowPosition(num_rows)

        return (html_lines, error_string)

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlGenericListParser"
