#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from ..Parser import Parser

class Markdown2HtmlImageLinkParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next character, and identify image links for conversion to HTML.
    def Parse(self, text_rows):
        html_lines = []
        link = ""
        link_text = ""
        row = text_rows.GetRow()
        column_pos = text_rows.GetColumnPosition()

        if row.startswith("![", column_pos):
            close_bracket = row.find("]", column_pos)
            if close_bracket > column_pos + 2:
                link_text = row[column_pos + 2:close_bracket]
            open_bracket = row.find("(", column_pos)
            if open_bracket > close_bracket:
                close_bracket = row.find(")", open_bracket + 1)
                if close_bracket > open_bracket + 1:
                    link = row[open_bracket + 1:close_bracket]
                    text_rows.SetColumnPosition(close_bracket + 1)

        if link:
            self.AddLink(link)
            if link_text:
                html_link = "<img src=\"{}\" alt=\"{}\">".format(link, link_text)
            else:
                html_link = "<img src=\"{}\"></br>".format(link)
            html_lines = [html_link]

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlImageLinkParser"
