#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlUtility import HtmlUtility

## Convert markdown table, where following line starts with "|"
class Markdown2HtmlTableParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next lines from the current row.
    def Parse(self, text_rows):
        html_lines = []

        row_pos = text_rows.GetRowPosition()
        header_line = text_rows.GetRow()
        align_line  = text_rows.GetRowAt(row_pos + 1)

        if header_line.startswith("|") and align_line.startswith("|-"):
            html_lines = ["<table>"]
            html_lines.append("  <tr>")

            # Add the column headers.
            for heading in header_line.split("|"):
                if heading:
                    tagged_line = HtmlUtility.GetTaggedString("th", heading.strip())
                    html_lines.append("    " + tagged_line)
            html_lines.append("  </tr>")

            # Get alignment for heading.
            row_pos += 1
            alignments = []
            for alignment in align_line.split("|"):
                if alignment:
                    if alignment.startswith(":") and alignment.endswith(":"):
                        html_align = " style=text-align:center"
                    elif alignment.startswith(":"):
                        html_align = " style=text-align:left"
                    elif alignment.endswith(":"):
                        html_align = " style=text-align:right"
                    else:
                        html_align = ""
                    alignments.append(html_align)

            # Add the rows.
            row_pos += 1
            line = text_rows.GetRowAt(row_pos)

            while line.startswith("|"):
                html_lines.append("  <tr>")
                column = 0
                for cell in line.split("|"):
                    if cell:
                        tagged_line = "<td{}>".format(alignments[column]) + \
                                      cell.strip() + "</td>"
                        html_lines.append("    " + tagged_line)
                        column += 1
                html_lines.append("  </tr>")
                row_pos += 1
                line = text_rows.GetRowAt(row_pos)

            html_lines.append("</table>")
            text_rows.SetRowPosition(row_pos)

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlTableParser"
