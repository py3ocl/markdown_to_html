#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser

## Convert markdown strike-through text to HTML, where line starts with "`"
class Markdown2HtmlInlineCodeParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next character, and identify inline code characters for conversion to HTML.
    def Parse(self, text_rows):
        html_lines = []
        inline_text = ""
        row = text_rows.GetRow()
        column_pos = text_rows.GetColumnPosition()

        if row.startswith("`", column_pos):
            close_bold = row.find("`", column_pos + 1)
            if close_bold > column_pos + 1:
                inline_text = row[column_pos + 1:close_bold]
                text_rows.SetColumnPosition(close_bold + 1)

        if inline_text:
            html_text = "<code>{}</code>".format(inline_text)
            html_lines = [html_text]

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlInlineCodeParser"
