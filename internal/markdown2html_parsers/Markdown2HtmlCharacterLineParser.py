#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser

## When the parser has not handles formatting that starts at the beginning of a line,
## then this parser will parse each character on remaining lines and perform embedded formatting.
class Markdown2HtmlCharacterLineParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self, embedded_parsers, terminating_characters = None):
        Parser.__init__(self, True)

        # Character line parser iterates embedded parsers to convert a line from Markdown to HTML.
        self._embedded_parsers = embedded_parsers

        # List of characters that can terminate the parsing of embedded characters early.
        # This allows for a mixture of embedded characters within tables, etc.
        self._terminating_characters = [] if terminating_characters is None else terminating_characters

    ## Iterate the embedded parsers to find the embedded Markdown text to be converted to HTML.
    def GetEmbeddedText(self, text_rows):
        # When the first parser can converted partial Markdown text for an embedded line,
        # set the partial convert HTML line from the first line in the HTML lines array.
        html_lines = []

        for parser in self._embedded_parsers:
            html_lines = parser.Parse(text_rows)

            if html_lines:
                # Transfer any links from the embedded parsers to the character line parser.
                # They need transferring as the embedded parsers are private to the Parsers class.
                self.AddLinks(parser.GetLinks())
                parser.ClearLinks()

                # Embedded parser should only return 1 row, otherwise it's wrong.
                if len(html_lines) != 1:
                    html_lines = []
                break

        return html_lines

    ## Parse the next lines from the current row.
    def Parse(self, text_rows):
        row = text_rows.GetRow()
        html_line = ""

        # Handle all characters in an embedded row.
        while text_rows.GetColumnPosition() < len(row):
            html_lines = self.GetEmbeddedText(text_rows)
            if html_lines and len(html_lines) == 1:
                html_line += html_lines[0]
            else:
                # Each character not parsed, add the raw character to the row end.
                html_line += row[text_rows.GetColumnPosition()]
                text_rows.IncrementColumnPosition()

        text_rows.IncrementRowPosition()
        text_rows.SetColumnPosition(0)

        return [html_line] if html_line else []

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlCharacterLineParser"
