#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlUtility import HtmlUtility

## Convert markdown header 2, where following line starts with "--"
class Markdown2HtmlHeader2Parser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next 2 markdown lines, where the second row starts with "--".
    def Parse(self, text_rows):
        rows = text_rows.GetRows(True, 2)
        html_lines = []

        if len(rows) == 2 and len(rows[0].strip()) > 0 and rows[1].startswith("--"):
            tagged_line = HtmlUtility.GetTaggedString("h2", rows[0])
            html_lines = [tagged_line]
            text_rows.IncrementRowPosition(2)

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlHeader2Parser"
