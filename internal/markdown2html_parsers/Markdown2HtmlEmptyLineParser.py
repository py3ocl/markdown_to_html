#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ..Parser import Parser
from ..HtmlUtility import HtmlUtility

class Markdown2HtmlEmptyLineParser(Parser):

    ## Initialise with TextRows object containing Markdown lines.
    def __init__(self):
        Parser.__init__(self)

    ## Parse the next line from the current row.
    def Parse(self, text_rows):
        html_lines = []
        rows = text_rows.GetRows()

        if text_rows.GetRowPosition() < len(rows) and \
           not rows[text_rows.GetRowPosition()].strip():

            text_rows.SetColumnPosition(0)

            # Skip all continuous blank lines.
            while text_rows.GetRowPosition() < len(rows) and \
                  not rows[text_rows.GetRowPosition()].strip():

                text_rows.IncrementRowPosition()

            html_lines = [HtmlUtility.GetMultilineStyleStart()]
            html_lines.append(HtmlUtility.GetMultilineStyleEnd())

        return html_lines

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Markdown2HtmlEmptyLineParser"
