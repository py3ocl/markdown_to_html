#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class HtmlCharacterMap:

    ## List of character codes not handled by Lookup function.
    NON_BREAKING_SPACE             = "&nbsp;"
    INVERTED_EXCLAMATION_MARK      = "&iexcl;"
    CURRENCY_SIGN                  = "&curren;"
    YEN_SIGN                       = "&yen;"
    BROKEN_BAR                     = "&brvbar;"   # Vertical broken bar
    SECTION_SIGN                   = "&sect;"
    DIAERESIS                      = "&uml;"
    FEMININE_ORDINAL_INDICATOR     = "&ordf;"     # Raised small a character
    LEFT_POINTING_GUILLEMET        = "&laquo;"    # Double left arrow
    RIGHT_POINTING_GUILLEMET       = "&raquo;"    # Double right arrow
    SOFT_HYPHEN                    = "&shy;"
    MACRON                         = "&macr;"     # Raised horizontal bar
    SUPERSCRIPT_ONE                = "&sup1;"     # Raised 1
    SUPERSCRIPT_TWO                = "&sup2;"     # Raised 2
    SUPERSCRIPT_THREE              = "&sup3;"     # Raised 3
    ACUTE_ACCENT                   = "&acute;"
    MICRO_SIGN                     = "&micro;"
    PARAGRAPH_SIGN                 = "&para;"
    MIDDLE_DOT                     = "&middot;"
    SPACING_CEDILLA                = "&cedil;"
    MASCULINE_ORDINAL_INDICATOR    = "&ordm;"

    ## Currency symbols.
    CENT                           = "&cent;"
    POUND                          = "&pound;"
    DOLLAR                         = "&#36;"
    YEN                            = "&yen;"
    CENT                           = "&cent;"
    INDIAN_RUPEE                   = "&#8377;"
    RUPEE                          = "&#8360;"
    PESO                           = "&#8369;"
    KOREAN_WON                     = "&#8361;"
    THAI_BAHT                      = "&#3647;"
    DONG                           = "&#8363;"
    SHEKEL                         = "&#8362;"

    ## Mathematic symbols
    AMPERSAND                      = "&amp;"
    BULLET                         = "&bull;"
    WHITE_BULLET                   = "&#9702;"
    BULLET_OPERATOR                = "&#8729;"
    TRIANGULAR_BULLET              = "&#8227;"
    MULTIPLICATION_DOT             = "&sdot;"
    HYPHEN_BULLET                  = "&#8259;"
    DEGREE_SIGN                    = "&deg;"      # Degree temperature
    INFINITY                       = "&infin;"    # Rotate 90 degrees 8 digit
    PROPORTIONAL_TO                = "&prop;"     # Rotate 90 degrees 8 digit with missing right side
    PER_MILLE                      = "&permil;"
    PER_TEN_THOUSAND               = "&pertenk;"
    HERMITIAN                      = "&dagger;"
    LONG_DASH                      = "&mdash;"
    NOT_SIGN                       = "&not;"      # Horizontal line
    MICRO                          = "&#8212;"
    PERPENDICULAR                  = "&perp;"     # Perpendicular lines (90 degree angle)
    PARALLEL                       = "&par;"      # Double vertical lines
    NOT_PARALLEL_TO                = "&npar;"     # Double vertical lines with line through
    LOGICAL_AND                    = "&and;"      # Upside down capital V character
    LOGICAL_OR                     = "&or;"       # Capital V character
    INTERSECTION                   = "&cap;"      # Upside down capital U character
    UNION                          = "&cup;"      # Capital U character
    SUBSET_OF                      = "&sub;"      # Uppercase U character rotated right
    SUPERSET_OF                    = "&sup;"      # Uppercase U character rotated left
    NOT_A_SUBSET_OF                = "&nsub;"     # Uppercase U character rotated right and line through
    NOT_A_SUPERSET_OF              = "&nsup;"     # Uppercase U character rotated left and line through
    SUBSET_OF_OR_EQUAL_TO          = "&sube;"     # Uppercase U character rotated right with horizontal line below
    SUPERSET_OF_OR_EQUAL_TO        = "&supe;"     # Uppercase U character rotated left with horizontal line below
    NOT_A_SUBSET_OF_OR_EQUAL_TO    = "&nsube;"    # Uppercase U character rotated right with horizontal line below and line through
    NOT_A_SUPERSET_OF_OR_EQUAL_TO  = "&nsupe;"    # Uppercase U character rotated left with horizontal line below and line through
    SUBSET_OF_WITH_NOT_EQUAL_TO    = "&subne;"    # Uppercase U character rotated right with horizontal line below and line through horizontal line
    SUPERSET_OF_WITH_NOT_EQUAL_TO  = "&supne;"    # Uppercase U character rotated left with horizontal line below and line through horizontal line
    MULTISET                       = "&#8844;"    # Capital U character with left arrow within centre of character
    MULTISET_MULTIPLICATION        = "&cupdot;"   # Capital U character with dot within centre of character
    MULTISET_UNION                 = "&uplus;"    # Capital U character with cross within centre of character
    INTEGRAL                       = "&int;"      # Large lowercase f with missing horizontal line
    DOUBLE_INTEGRAL                = "&Int;"      # Double integral
    TRIPLE_INTEGRAL                = "&iiint;"    # Triple integral
    CONTOUR_INTEGRAL               = "&conint;"   # Integral character with circle through the middle
    SURFACE_INTEGRAL               = "&Conint;"   # Double integral character with circle through the middle
    VOLUME_INTEGRAL                = "&Cconint;"  # Triple integral character with circle through the middle
    CLOCKWISE_INTEGRAL             = "&cwint;"    # Integral character with arrowed line through the middle
    CLOCKWISE_CONTOUR_INTEGRAL     = "&cwconint;" # Integral character with arrowed circle through the middle
    ANTICLOCKWISE_CONTOUR_INTEGRAL = "&awconint;" # Integral character with anti-clockwise arrowed circle through the middle
    THEREFORE                      = "&there4;"   # Three dots in a triangle form
    BECAUSE                        = "&because;"  # Three dots in an upside down triangle form
    RATIO                          = "&ratio;"    # Same as colon
    PROPORTION                     = "&Colon;"    # Two ration symbols
    DOT_MINUS                      = "&minusd;"   # Minus with dot centred about line
    EXCESS                         = "&#8761;"    # Minus with colon at end
    GEOMETRIC_PROPORTION           = "&mDDot;"    # Minus with dots above and below on left and right
    HOMOTHETIC                     = "&homtht;"   # Tilde with centred dot above and below
    TILDE                          = "&sim;"
    NOT_TILDE                      = "&nsim;"     # Tilde with line though
    REVERSED_TILDE                 = "&bsim;"
    MINUS_TILDE                    = "&esim;"     # Tilde with minus symbol above
    ASYMPTOTICALLY_EQUAL_TO        = "&sime;"     # Tilde with minus symbol below
    NOT_ASYMPTOTICALLY_EQUAL_TO    = "&nsime;"    # Tilde with minus symbol below and line through
    INVERTED_LAZY_S                = "&ac;"
    WREATH_PRODUCT                 = "&wreath;"   # Tilde rotated 90 degrees.
    SINE_WAVE                      = "&acd;"
    FRACTION_ONE_QUARTER           = "&frac14;"
    FRACTION_ONE_HALF              = "&frac12;"
    FRACTION_THREE_QUARTERS        = "&frac34;"
    SQUARE_ROOT                    = "&radic;"
    CUBE_ROOT                      = "&#8731;"
    FOURTH_ROOT                    = "&#8732;"
    RIGHT_ANGLE                    = "&angrt;"
    RIGHT_ANGLE_WITH_ARC           = "&angrtvb;"
    RIGHT_TRIANGLE                 = "&lrtri;"
    ANGLE                          = "&ang;"
    MEASURED_ANGLE                 = "&angmsd;"
    SPHERICAL_ANGLE                = "&angsph;"   # Rotated measured angle
    XOR                            = "&veebar;"   # Lowercase v with horizontal line below
    NAND                           = "&#8892;"    # XOR rotated 180 degrees
    NOR                            = "&barvee;"   # Lowercase v with horizontal line above
    NARY_LOGICAL_AND               = "&xwedge;"   # Upside down capital V character
    NARY_LOGICAL_OR                = "&xvee;"     # Capital V character
    NARY_INTERSECTION              = "&xcap;"     # Upside down U character
    NARY_UNION                     = "&xcup;"     # U character
    DIVIDES                        = "&mid;"      # Long vertical line
    DOES_NOT_DIVIDE                = "&nmid;"     # Long vertical line with line through
    PLUS_MINUS                     = "&plusmn;"   # Plus symbol above minus symbol
    MINUS_PLUS                     =  "&mnplus;"  # Minus symbol above plus symbol
    DOT_PLUS                       = "&plusdo;"   # Plus symbol with dot above
    FUNCTION                       = "&#402;"
    FOR_ALL                        = "&forall;"   # Upside down A character
    COMPLEMENT                     = "&comp;"     # Large capital C character
    PARTIAL_DIFFERENTIAL           = "&part;"
    THERE_EXISTS                   = "&exist;"    # Reversed capital E character
    THERE_DOES_NOT_EXIST           = "&nexist;"   # Reversed capital E character with line through
    EMPTY_SET                      = "&empty;"    # Zero with line through
    INCREMENT                      = "&#8710;"    # Large hollow triangle
    NABLA                          = "&nabla;"    # Large hollow upside down triangle
    ELEMENT_OF                     = "&isin;"     # Rounded large capital E character
    NOT_AN_ELEMENT_OF              = "&notin;"    # Rounded large E character with line through
    SMALL_ELEMENT_OF               = "&#8714;"    # Rounded smaller capital E character
    CONTAINS_AS_MEMBER             = "&ni;"       # Reversed rounded large capital E character
    DOES_NOT_CONTAIN_AS_MEMBER     = "&notni;"    # Reversed rounded large capital E character with line through
    SMALL_CONTAINS_AS_MEMBER       = "&#8717;"    # Reversed rounded smaller capital E character
    END_OF_PROOF                   = "&#8718;"    # Solid filled square
    NARY_PRODUCT                   = "&coprod;"   # N-Ary Product two capital I characters with joining line at base
    NARY_SUMMATION                 = "&sum;"      # Sum symbol

    ## Intellectual property codes.
    COPYRIGHT                      = "&copy;"
    REGISTERED_SIGN                = "&reg;"
    SOUND_RECORDING_COPYRIGHT      = "&#8471;"
    TRADEMARK                      = "&trade;"
    SERVICE_MARK                   = "&#8480;"

    ## Initialise list of common mapped characters.
    def __init__(self):
        self._escape_dict = {
            '\t' : "&#09;",
            '\n' : "&#10;",
            '\r' : "&#13;",
            ' ' : "&#32;",
            '!' : "&#33;",
            "\"" : "&quot;",
            '#' : "&#35;",
            '$' : "&#36;",
            '%' : "&#37;",
            '&' : "&amp;",
            '\'' : "&#39;",
            '(' : "&#40;",
            ')' : "&#41;",
            '*' : "&#42;",
            '+' : "&#43;",
            ',' : "&#44;",
            '-' : "&#45;",
            '.' : "&#46;",
            '/' : "&#47;",
            '0' : "&#48;",
            '1' : "&#49;",
            '2' : "&#50;",
            '3' : "&#51;",
            '4' : "&#52;",
            '5' : "&#53;",
            '6' : "&#54;",
            '7' : "&#55;",
            '8' : "&#56;",
            '9' : "&#57;",
            ':' : "&#58;",
            ';' : "&#59;",
            '<' : "&lt;",
            '=' : "&#61;",
            '>' : "&gt;",
            '?' : "&#63;",
            '@' : "&#64;",
            '[' : "&#91;",
            '\\' : "&#92;",
            ']' : "&#93;",
            '^' : "&#94;",
            '_' : "&#95;",
            '`' : "&#96;",
            '{' : "&#123;",
            '|' : "&#124;",
            '}' : "&#125;",
            '~' : "&#126;" }

    ## Map the character to the HTML escape code.
    def Lookup(self, char_to_map):
        escape_str = self._escape_dict.get(char_to_map)

        # 'A'..'Z' and 'a'..'z' will calculate the escape string.
        if escape_str is None:
            if (char_to_map >= 'A' and char_to_map <= 'Z') or \
               (char_to_map >= 'a' and char_to_map <= 'z'):

                escape_str = "&#{};".format(ord(char_to_map))
            else:
                escape_str = ""

        return escape_str

    ## Escape all characters and return the escaped string.
    def Escape(self, string_to_escape):
        escaped_string = ""

        for c in string_to_escape:
            escaped_char = self.Lookup(c)
            if escaped_char:
                escaped_string += escaped_char

        return escaped_string
