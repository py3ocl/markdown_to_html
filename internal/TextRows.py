#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .FileUtility import FileUtility

## Managed list of text rows, with a column position for current row.
class TextRows:

    ## Initialise row and column positions.
    def __init__(self, rows = None):
        self._row_pos = 0
        self._column_pos = 0
        self._rows = rows if rows else []

    ## Reset the positions back to the start.
    def ResetPositions(self):
        self._row_pos = 0
        self._column_pos = 0

    ## Get the current row position.
    def GetRowPosition(self):
        return self._row_pos

    ## Set the current row position.
    def SetRowPosition(self, row):
        self._row_pos = row

    ## Increment the current row position by a specified amount.
    def IncrementRowPosition(self, inc_by = 1):
        self._row_pos += inc_by

    ## Get the current column position.
    def GetColumnPosition(self):
        return self._column_pos

    ## Set the current column position.
    def SetColumnPosition(self, column):
        self._column_pos = column

    ## Increment the current column position by a specified amount.
    def IncrementColumnPosition(self, inc_by = 1):
        self._column_pos += inc_by

    ## Get number of rows.
    def GetRowCount(self):
        return len(self._rows)

    ## Get list of all the rows.
    ## Optionally get rows from the current row position and count.
    def GetRows(self, start_from_row_position = False, count = None):
        if count:
            return self._rows[self._row_pos:self._row_pos + count] \
                   if start_from_row_position else self._rows[:count]

        return self._rows[self._row_pos:] if start_from_row_position else self._rows

    ## Set list of rows.
    def SetRows(self, rows):
        self._rows = rows

    ## Get list of the rows from start to the end, unless count is specified.
    def GetRowsAt(self, start, count = None):
        return self._rows[start:start + count] if count else self._rows[start:]

    ## Get the current row of text.
    def GetRow(self, start_from_column_position = False):
        row = self._rows[self._row_pos] if self._row_pos < len(self._rows) else ""
        return row[self._column_pos:] if start_from_column_position else row

    ## Get row at a position.
    def GetRowAt(self, row_pos, start_from_column_position = False):
        row = self._rows[row_pos] if row_pos < len(self._rows) else ""
        return row[self._column_pos:] if start_from_column_position else row

    ## Add text to the rows.
    def AddRow(self, text):
        self._rows.append(text)

    ## Append text to the current row.
    def AppendToRow(self, text):
        while len(self._rows) <= self._row_pos:
            self._rows.append("")
        self._rows[self._row_pos] = self._rows[self._row_pos] + text

    ## Read a file and set the rows.
    ## Return an error string if the file could not be read.
    def ReadFile(self, infile):
        (rows, error) = FileUtility.Read(infile)
        if not error:
            self._rows = rows
        return error

    ## Write the rows to a file.
    ## Return an error string if the file could not be written.
    def WriteFile(self, outfile):
        return FileUtility.Write(outfile, self._rows)
