#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class StringUtility:

    ## Helper method to count characters at the start of text.
    @staticmethod
    def CountStartChars(text, ch):
        count = 0
        while len(text) > count and text[count] == ch:
            count += 1
        return count

    ## Get the numeric part of the string, after removing while space
    ## up to a specific character to find.
    @staticmethod
    def GetNumericString(numeric_string, char_to_find):
        ret_str = ""
        numeric_string = numeric_string.strip()
        pos = numeric_string.find(char_to_find)

        if pos != -1:
            ret_str = numeric_string[:pos]
            for c in ret_str:
                if not c.isnumeric():
                    ret_str = ""
                    break
        return ret_str

    ## Simple boolean check to convert GetNumericString into True or False.
    @staticmethod
    def IsNumericString(numeric_string, char_to_find):
        ret_str = StringUtility.GetNumericString(numeric_string, char_to_find)
        return False if not ret_str else True

    ## Indent the lines with spaces, defaulting to 2 spaces when not specified.
    @staticmethod
    def IndentLines(lines, indent = None):
        if type(indent) is int:
            indent = " " * indent
        if not indent:
            indent = "  "
        index = 0
        while index < len(lines):
            lines[index] = indent + lines[index]
            index += 1

    ## Convert binary data read from file to a list of text lines, split by '\n'.
    @staticmethod
    def ConvertBinaryDataToLines(data):
        lines = []

        for line in data.split("\n"):
            if line.endswith('\r'):
                line = line[:-1]
            lines.append(line)

        # Remove any blank trailing lines.
        while lines and not lines[-1]:
            lines.pop()

        return lines

