#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import platform
import base64
from .Parsers import Parsers
from internal.HtmlUtility import HtmlUtility
from .FileUtility import FileUtility
from .markdown2html_parsers.Markdown2HtmlEmptyLineParser import Markdown2HtmlEmptyLineParser
from .markdown2html_parsers.Markdown2HtmlHeadersParser import Markdown2HtmlHeadersParser
from .markdown2html_parsers.Markdown2HtmlHeader1Parser import Markdown2HtmlHeader1Parser
from .markdown2html_parsers.Markdown2HtmlHeader2Parser import Markdown2HtmlHeader2Parser
from .markdown2html_parsers.Markdown2HtmlMultilineCodeBlockParser import Markdown2HtmlMultilineCodeBlockParser
from .markdown2html_parsers.Markdown2HtmlTableParser import Markdown2HtmlTableParser
from .markdown2html_parsers.Markdown2HtmlLineParser import Markdown2HtmlLineParser
from .markdown2html_parsers.Markdown2HtmlBlockquoteParser import Markdown2HtmlBlockquoteParser
from .markdown2html_parsers.Markdown2HtmlUnorderedListParser import Markdown2HtmlUnorderedListParser
from .markdown2html_parsers.Markdown2HtmlOrderedListParser import Markdown2HtmlOrderedListParser
from .markdown2html_parsers.Markdown2HtmlCheckboxListParser import Markdown2HtmlCheckboxListParser
from .markdown2html_parsers.Markdown2HtmlLinkParser import Markdown2HtmlLinkParser
from .markdown2html_parsers.Markdown2HtmlImageLinkParser import Markdown2HtmlImageLinkParser
from .markdown2html_parsers.Markdown2HtmlEscapeParser import Markdown2HtmlEscapeParser
from .markdown2html_parsers.Markdown2HtmlEmojiParser import Markdown2HtmlEmojiParser
from .markdown2html_parsers.Markdown2HtmlBoldParser import Markdown2HtmlBoldParser
from .markdown2html_parsers.Markdown2HtmlItalicParser import Markdown2HtmlItalicParser
from .markdown2html_parsers.Markdown2HtmlStrikethroughParser import Markdown2HtmlStrikethroughParser
from .markdown2html_parsers.Markdown2HtmlInlineCodeParser import Markdown2HtmlInlineCodeParser
from .markdown2html_parsers.Markdown2HtmlCharacterLineParser import Markdown2HtmlCharacterLineParser

## Collection of markdown to HTML parsers.
class Markdown2HtmlParsers(Parsers):

    DEFAULT_BORDER_STYLE = "border-style:solid;"

    ## Switches (starting with --)
    KEY_TITLE         = "title"
    KEY_CSS_IMPORT    = "css-import"
    KEY_CSS_INCLUDE   = "css-include"
    KEY_CSS_EXPORT    = "css-export"
    KEY_BORDER_STYLE  = "border-style"
    KEY_CHARSET       = "charset"
    KEY_LANGUAGE      = "language"
    KEY_AUTO_NEW_LINE = "auto-new-line"
    KEY_RECURSIVE     = "recursive"
    KEY_VERBOSE       = "verbose"

    ## Keys used for input and output filenames.
    KEY_INPUT_FILE    = "input-file"
    KEY_OUTPUT_FILE   = "output-file"
    KEY_INPUT_DIR     = "input-dir"
    KEY_OUTPUT_DIR    = "output-dir"

    ## Keys for storing other information.
    KEY_CSS_LINES     = "css-lines"

    ## Setup any base class data.
    def __init__(self):
        super().__init__()

        # Set the default values for the keys for use with arguments and extracting values for start of HTML document.
        self.SetSwitch(Markdown2HtmlParsers.KEY_TITLE, "DUMMY TITLE", "Title of the page")
        self.SetSwitch(Markdown2HtmlParsers.KEY_CSS_IMPORT, "", "Name of .css file for import")
        self.SetSwitch(Markdown2HtmlParsers.KEY_CSS_INCLUDE, "", "Name of .css file to be included")
        self.SetSwitch(Markdown2HtmlParsers.KEY_CSS_EXPORT, "", "Name of exported.css file")
        self.SetSwitch(Markdown2HtmlParsers.KEY_BORDER_STYLE, "", "Custom border style for all elements")
        self.SetSwitch(Markdown2HtmlParsers.KEY_CHARSET, "", "Character set for HTML document, e.g. utf-8")
        self.SetSwitch(Markdown2HtmlParsers.KEY_LANGUAGE, "", "Language for HTML document, e.g. \"en\"")
        self.SetSwitch(Markdown2HtmlParsers.KEY_AUTO_NEW_LINE, False, "Every line is on it's own line in the HTML document")
        self.SetSwitch(Markdown2HtmlParsers.KEY_RECURSIVE, False, "Recursively convert all documents")
        self.SetSwitch(Markdown2HtmlParsers.KEY_VERBOSE, False, "Enable output of filenames while converting files")

        # Set additional values that will be used that are not part of the - or -- switches.
        self.SetAttribute(Markdown2HtmlParsers.KEY_INPUT_FILE, "")
        self.SetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_FILE, "")
        self.SetAttribute(Markdown2HtmlParsers.KEY_CSS_LINES, [])
        self.SetAttribute(Markdown2HtmlParsers.KEY_INPUT_DIR, "")
        self.SetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_DIR, "")

        self._verbose = False

    ## Override the base class SetParsers function to add the parsers, and return True.
    def SetParsers(self):

        # All parsers that will parse markdown that is a self contain block,
        # and not mixed with other unformatted text.
        block_parsers = [Markdown2HtmlHeadersParser(),
                         Markdown2HtmlHeader1Parser(),
                         Markdown2HtmlHeader2Parser(),
                         Markdown2HtmlMultilineCodeBlockParser(),
                         Markdown2HtmlTableParser(),
                         Markdown2HtmlLineParser(),
                         Markdown2HtmlBlockquoteParser(),
                         Markdown2HtmlUnorderedListParser(),
                         Markdown2HtmlOrderedListParser(),
                         Markdown2HtmlCheckboxListParser(),

                         # Any remaining empty lines must be parsed last,
                         # so lines, etc can be parsed first.
                         Markdown2HtmlEmptyLineParser()]

        # Parsers that can be embedded within a line, to mix with unformatted text.
        # NOTE: The character parser will iterate the embedded parsers
        #       during Markdown to HTML conversion.
        embedded_parsers = [Markdown2HtmlLinkParser(),
                            Markdown2HtmlImageLinkParser(),
                            Markdown2HtmlEscapeParser(),
                            Markdown2HtmlEmojiParser(),
                            Markdown2HtmlBoldParser(),
                            Markdown2HtmlItalicParser(),
                            Markdown2HtmlStrikethroughParser(),
                            Markdown2HtmlInlineCodeParser()]

        ## Set the character parser to iterate all the embedded parsers,
        ## when block parsers do not convert rows.
        character_parser = Markdown2HtmlCharacterLineParser(embedded_parsers)

        self.AddParsers(block_parsers)
        self.AddParser(character_parser)

        return True

    ## Get the list of strings that will be written at the start of the HTML file.
    def GetDocumentStart(self):
        border_style = self.GetSwitch(Markdown2HtmlParsers.KEY_BORDER_STYLE)
        if type(border_style) is bool:
            border_style = "border: 1px solid black;" if border_style else ""
        return HtmlUtility.GetDocumentStart(self.GetSwitch(Markdown2HtmlParsers.KEY_TITLE),
                                            self.GetSwitch(Markdown2HtmlParsers.KEY_CHARSET),
                                            self.GetSwitch(Markdown2HtmlParsers.KEY_LANGUAGE),
                                            self.GetSwitch(Markdown2HtmlParsers.KEY_CSS_LINES),
                                            border_style)

    ## Get the list of strings that will be written at the end of the HTML file.
    def GetDocumentEnd(self):
        return HtmlUtility.GetDocumentEnd()

    ## Get the start string for embedded lines.
    def GetEmbeddedStart(self):
        return HtmlUtility.GetMultilineStyleStart()

    ## Get the ending string for embedded lines.
    def GetEmbeddedEnd(self):
        return HtmlUtility.GetMultilineStyleEnd()

    ## Parse the embedded rows to see if the needed embedded start/end adding.
    def WrapEmbeddedRows(self, embedded_rows):
        for r in embedded_rows:
            if r.endswith("  "):
                return True
        return False

    ## Override function to replace the filename extension for input and output files.
    def ReplaceExtension(self, filename):
        return FileUtility.ReplaceFilenameExtension(filename, ".md", ".html")

    ## Return True if verbose output, when overridden.
    def IsVerbose(self):
        return self._verbose

    ## Convert all Markdown lines to HTML lines.
    def ConvertMarkdownToHtml(self):
        self._verbose = self.GetSwitch(Markdown2HtmlParsers.KEY_VERBOSE)
        recursive = self.GetSwitch(Markdown2HtmlParsers.KEY_RECURSIVE)
        self.SetRecursive(recursive)
        self.SetParsers()

        if recursive:
            input_dir = self.GetAttribute(Markdown2HtmlParsers.KEY_INPUT_DIR)
            output_dir = self.GetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_DIR)
            error = self.RecursiveConvert(input_dir, output_dir, ".md")
        else:
            # Not recursive, so just convert the one file.
            infile = self.GetAttribute(Markdown2HtmlParsers.KEY_INPUT_FILE)
            outfile = self.GetAttribute(Markdown2HtmlParsers.KEY_OUTPUT_FILE)
            if os.path.isdir(infile):
                error = "Input file {} is a directory. Did you mean to use --recursive?".format(infile)
            else:
                error = self.Convert(infile, outfile)

        if not error:
            error = self.CopyLinks()

        return error