#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class MarkdownEmoji2Html:

    ## Initialise list of common mapped emojis.
    def __init__(self):
        self._emoji_dict = {
            "+1"                            : "&#128077;",
            "-1"                            : "&#58401;",
            "alien"                         : "&#128125;",
            "angry"                         : "&#128544;",
            "astonished"                    : "&#128558;",
            "blue_heart"                    : "&#128153;",
            "boom"                          : "&#128165;",
            "broken_heart"                  : "&#128148;",
            "check_mark"                    : "&#10004;",
            "collision"                     : "&#128165;",
            "confounded"                    : "&#128534;",
            "cross_mark"                    : "&#10060;",
            "cupid"                         : "&#128152;",
            "disappointed"                  : "&#128542;",
            "dollar_sign"                   : "&#128178;",
            "exclamation"                   : "&#10071;",
            "exclamation_mark"              : "&#10071;",
            "expressionless"                : "&#128529;",
            "fearful"                       : "&#128552;",
            "fist"                          : "&#9994;",
            "flushed"                       : "&#128563;",
            "frowning"                      : "&#128550;",
            "gem"                           : "&#128142;",
            "green_heart"                   : "&#128154;",
            "grimacing"                     : "&#128556;",
            "grin"                          : "&#x1F601;",
            "grinning"                      : "&#x1F604;",
            "hearts"                        : "&hearts;",
            "heartbeat"                     : "&#128147;",
            "heartpulse"                    : "&#128151;",
            "heart_eyes"                    : "&#128525;",
            "heavy_check_mark"              : "&#10004;&#65039;",
            "heavy_exclamation_mark"        : "&#10071;",
            "imp"                           : "&#128127;",
            "innocent"                      : "&#128519;",
            "joy"                           : "&#128514;",
            "kissing"                       : "&#128535;",
            "kissing_heart"                 : "&#128536;",
            "laughing"                      : "&#128513;",
            "mask"                          : "&#128567;",
            "pray"                          : "&#128591;",
            "purple_heart"                  : "&#128156;",
            "question"                      : "&#10067;",
            "question_mark"                 : "&#10067;",
            "rage"                          : "&#128545;",
            "raised_hand"                   : "&#9995;",
            "red_heart"                     : "&#10084;&#65039;",
            "relaxed"                       : "&#128522;",
            "revolving_hearts"              : "&#128158;",
            "ring"                          : "&#128141;",
            "rolling_on_the_floor_laughing" : "&#129315;",
            "rofl"                          : "&#129315;",
            "scream"                        : "&#128561;",
            "sleeping"                      : "&#128517;",
            "sleepy"                        : "&#128554;",
            "smile"                         : "&#128512;",
            "smiley"                        : "&#x1F603;",
            "smiling_imp"                   : "&#128520;",
            "smirk"                         : "&#128527;",
            "sob"                           : "&#128557;",
            "sparkles"                      : "&#10024;",
            "sparkling_heart"               : "&#128150;",
            "star"                          : "&#11088;",
            "stuck_out_tongue"              : "&#128539;",
            "stuck_out_tongue_closed_eyes"  : "&#128523;",
            "stuck_out_tongue_winking_eye"  : "&#128540;",
            "sunglasses"                    : "&#128526;",
            "sweat_smile"                   : "&#128517;",
            "thumbsdown"                    : "&#58401;",
            "thumbsup"                      : "&#128077;",
            "tongue_poke"                   : "&#128539;",
            "two_hearts"                    : "&#128149;",
            "unamused"                      : "&#128530;",
            "wink"                          : "&#128521;",
            "worried"                       : "&#128543;",
            "x"                             : "&#10060;",
            "yellow_heart"                  : "&#128155;" }

    ## Get the dictionary for Markdown emoji keys and associated HTML codes.
    def GetDictionary(self):
        return self._emoji_dict

    ## Map the Markdown emoji to the HTML code.
    def Lookup(self, emoji_to_map):
        emoji_str = self._emoji_dict.get(emoji_to_map)

        return emoji_str if emoji_str else ""
