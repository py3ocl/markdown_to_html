#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import shutil
import traceback
from .Parser import Parser
from .TextRows import TextRows
from .FileUtility import FileUtility

class Parsers:

    ## Initialise the empty parser list.
    def __init__(self):
        self._parsers = []
        self._is_embedded = False

        # Set empty dictionary, which is used to store command line switches.
        self._switches = {}
        self._switches_desc = {}

        # Set empty dictionary, which will be used to set attributes used
        # for supporting parsing of documents.
        self._attributes = {}

        ## URL links to be copied as part of converting documents.
        self._copy_links = {}

        self.recursive = False

    ## Add single parser to the list of current parsers.
    def AddParser(self, parser):
        assert(type(parser) != list)
        self._parsers.append(parser)

    ## Add list of parsers to the list of current parsers.
    def AddParsers(self, parsers):
        assert(type(parsers) == list)
        self._parsers += parsers

    ## Get the value of the switch for the key, or return None if the switch does not exist.
    def GetSwitch(self, key):
        return self._switches.get(key)

    ## Get the description of the switch for the key, or return None if the attribute does not exist.
    def GetSwitchDescription(self, key):
        return self._switches_desc.get(key)

    ## Add switch which can be used to look up values while parsing documents.
    def SetSwitch(self, key, value, description = ""):
        self._switches[key] = value
        self._switches_desc[key] = description

    ## Get a list of the switches.
    def GetSwitches(self):
        return list(self._switches.keys())

    ## Get the attribute for the key, or return None if the attribute does not exist.
    def GetAttribute(self, key):
        return self._attributes.get(key)

    ## Add attributes which can be used to look up values while parsing documents.
    def SetAttribute(self, key, value):
        self._attributes[key] = value

    ## Get a list of the attribute keys.
    def GetAttributeKeys(self):
        return list(self._attributes.keys())

    ## Add a link with a source and destination path to dictionary for copying later.
    def AddCopyLink(self, link, src_path, dst_path):
        if src_path != dst_path:
            self._copy_links[os.path.join(src_path, link)] = os.path.join(dst_path, link)

    ## Add links from a list with a source and destination path to dictionary for copying later.
    def AddCopyLinks(self, links, src_path, dst_path):
        if src_path != dst_path:
            for link in links:
                self._copy_links[os.path.join(src_path, link)] = os.path.join(dst_path, link)

    ## Copy all the source links to destination links.
    def CopyLinks(self):
        for src_link in self._copy_links:
            dst_link = self._copy_links[src_link]
            try:
                if os.path.isfile(src_link):
                    if self.IsVerbose():
                        print("Copying {} to {}".format(src_link, dst_link))
                    shutil.copy(src_link, dst_link)
                elif self.IsVerbose():
                    print("Not copying {}".format(src_link))
            except:
                return "Failed to copy {} to {}\n".format(src_link, dst_link)
        return None

    ## Get the dictionary of copy links.
    def GetCopyLinks(self):
        return self._copy_links

    ## Iterator all parsers until one of the parsers parses one or more rows,
    ## then returns a list, otherwise an empty list is returned.
    def ParseRows(self, text_rows, src_path, dst_path):
        index = 0

        if not self._parsers:
            sys.stderr.write("No parsers found!\n")

        while index < len(self._parsers):
            parser = self._parsers[index]
            parser.ClearLinks()
            parsed_rows = parser.Parse(text_rows)
            links = parser.GetLinks()
            self.AddCopyLinks(links, src_path, dst_path)
            if parsed_rows:
                self._is_embedded = parser.IsEmbedded()
                return parsed_rows

            index += 1

        return []

    ## Add the embedded start and end to the embedded rows.
    ## If the embedded start/end are lists, then the rows are added to the start and end.
    ## If the embedded start/end are strings, they are added to the start of the first string,
    ## and to the end of the last string in the embedded rows.
    def AddEmbeddedStartAndEnd(self, embedded_rows, embedded_start, embedded_end):
        embedded_start = self.GetEmbeddedStart()

        if type(embedded_start) is list:
            for start in reversed(embedded_start):
                embedded_rows.insert(0, start)
        else:
            embedded_rows[0] = embedded_start + embedded_rows[0]

        embedded_end = self.GetEmbeddedEnd()
        if type(embedded_end) is list:
            embedded_rows += embedded_end
        else:
            embedded_rows[-1] += embedded_end

    ## Parse all rows by continuously calling Parse until no more rows are converted.
    def ParseAllRows(self, text_rows, src_path, dst_path):
        is_embedded = False
        parsed_rows = [""]
        embedded_parsed_rows = []
        all_parsed_rows = self.GetDocumentStart()

        while parsed_rows:
            parsed_rows = self.ParseRows(text_rows, src_path, dst_path)

            if parsed_rows:
                # Any embedded lines will be added at once with embedded start and end.
                if self._is_embedded:
                    is_embedded = True
                    embedded_parsed_rows += parsed_rows
                else:
                    # Add all embedded lines with start and end before non-embedded lines.
                    if is_embedded:
                        is_embedded = False
                        if self.WrapEmbeddedRows(embedded_parsed_rows):
                            self.AddEmbeddedStartAndEnd(embedded_parsed_rows,
                                                        self.GetEmbeddedStart(),
                                                        self.GetEmbeddedEnd())
                        all_parsed_rows += embedded_parsed_rows
                        embedded_parsed_rows = []

                    all_parsed_rows += parsed_rows

        if is_embedded:
            if self.WrapEmbeddedRows(embedded_parsed_rows):
                self.AddEmbeddedStartAndEnd(embedded_parsed_rows,
                                            self.GetEmbeddedStart(),
                                            self.GetEmbeddedEnd())
            all_parsed_rows += embedded_parsed_rows

        all_parsed_rows += self.GetDocumentEnd()

        return all_parsed_rows

    ## Set the parsers, by overriding this function in the derived class.
    ## Return False by default, and overriding function must return True on success.
    def SetParsers(self, text_rows):
        return False

    ## Reset all the parsers with a new TextRows object.
    def ResetParsers(self, markdown_text_rows):
        for parser in self._parsers:
            parser.SetTextRows(markdown_text_rows)
        return True if self._parsers else False

    ## Get the start of the output document as a list of strings,
    ## by overriding the function in the derived class.
    ## The starting rows will be written to the output file first,
    ## before parsed rows are written.
    ## Return [] by default, and overriding function must return list of strings.
    def GetDocumentStart(self):
        return []

    ## Get the end of the output document as a list of strings,
    ## by overriding the function in the derived class.
    ## The ending rows will be written to the output file last,
    ## after parsed rows are written.
    ## Return [] by default, and overriding function must return list of strings.
    def GetDocumentEnd(self):
        return []

    ## Get the start string for embedded lines, which is overridden in the derived class.
    def GetEmbeddedStart(self):
        return ""

    ## Get the ending string for embedded lines, which is overridden in the derived class.
    def GetEmbeddedEnd(self):
        return ""

    ## Parse the embedded rows to see if the needed embedded start/end adding.
    def WrapEmbeddedRows(self, embedded_rows):
        return False

    ## Override function to replace the filename extension for input and output files.
    def ReplaceExtension(self, filename):
        return filename

    ## Return True if verbose output, when overridden.
    def IsVerbose(self):
        return False

    ## @brief Return True if parsing recursively.
    def GetRecursive(self):
        return self.recursive

    ## @brief Set the parsers as recursive or not.
    def SetRecursive(self, is_recursive):
        self.recursive = is_recursive

    ## Read an input file, parse the text lines and output the parsed text lines.
    def Convert(self, infile, outfile):
        if self.IsVerbose():
            print("Converting {} to {}".format(infile, outfile))

        (rows, error) = FileUtility.Read(infile)
        if not error:
            if rows:
                text_rows = TextRows(rows)
                src_path = os.path.split(infile)[0]
                dst_path = os.path.split(outfile)[0]
                parsed_rows = self.ParseAllRows(text_rows, src_path, dst_path)

                if parsed_rows:
                    folder = os.path.split(outfile)[0]
                    if not folder or FileUtility.CreateDirectory(folder):
                        error = FileUtility.Write(outfile, parsed_rows)
                else:
                    error = "Parsers have not been set"
            else:
                error = "Empty input file {}".format(infile)

        return error

    ## Recursively read input files, parse them and output the parsed text lines.
    def RecursiveConvert(self, input_dir, output_dir, file_types):
        matched_files = []
        if FileUtility.FindFiles(matched_files, None, None, file_types, input_dir):
            for infile in matched_files:
                outfile = FileUtility.ReplacePath(self.ReplaceExtension(infile), input_dir, output_dir)
                error = self.Convert(infile, outfile)
                if error:
                    return error
        else:
            return "No matching files found in {}".format(input_dir)

        return None
