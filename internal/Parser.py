#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .TextRows import TextRows
from .FileUtility import FileUtility

class Parser:

    ## Initliase the Parser with the TextRows object.
    def __init__(self, is_embedded = False):
        self._is_embedded = is_embedded
        self._error_string = ""
        self._links = []

    ## Return true when the parser is parsing embedded syntax within a row,
    ## otherwise the parser is parsing blocks of text, i.e. rows.
    def IsEmbedded(self):
        return self._is_embedded

    ## Default implementation of Parse function that returns an empty list.
    ## Derived classes implement the Parse function.
    def Parse(self, text_rows):
        return []

    ## Get the name of the parser, which can be used to help with debugging.
    def Name(self):
        return "Base::Parser"

    ## Add a URL link to the list of links.
    def AddLink(self, link):
        self._links.append(link)

    ## Add a URL link to the list of links.
    def AddLinks(self, links):
        for link in links:
            self._links.append(link)

    ## Clear the URL links from the list.
    def ClearLinks(self):
        self._links.clear()

    ## Get a list of one of more URL links that have been parsed.
    def GetLinks(self):
        return self._links

    ## Get the error string, or an empty string when there is no error.
    def GetError(self):
        return self._error_string

    ## Set the error string.
    def SetError(self, error_string):
        self._error_string = error_string
