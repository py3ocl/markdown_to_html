#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .StringUtility import StringUtility

## Utility functions to help with generating HTML text.
class HtmlUtility:

    ## Wrap the tag into <> and </> which is placed around the line.
    @staticmethod
    def GetTaggedLine(tag, line):
        start_tag = "<" + tag + ">"
        end_tag = "</" + tag + ">"
        return start_tag + line + end_tag

    # Get the start multi-line style when set, and clear as it's returned.
    @staticmethod
    def GetMultilineStyleStart():
        return "<div class=\"multiline\">"

    # Get the end multi-line style when set, and clear as it's returned.
    @staticmethod
    def GetMultilineStyleEnd():
        return "</div>"

    ## Wrap the tag into <> and </> which is placed around the line.
    @staticmethod
    def GetTaggedString(tag, text):
        start_tag = "<" + tag + ">"
        end_tag = "</" + tag + ">"
        return start_tag + text + end_tag

    ## Get the HTML document start, up to and including <body> tag, as a list of text lines.
    ## NOTE: If charset is "" then the HTML element for meta charset will not be added.
    ## NOTE: Only css_include or css_lines will be used for CSS styles.
    @staticmethod
    def GetDocumentStart(title,
                         charset = None,
                         lang = None,
                         css_include = None,
                         css_lines = None,
                         border_style = None):

        if lang is None:
            lang = "en"

        if charset is None:
            charset = "utf-8"

        doc_start = [
            "<!DOCTYPE html>",
            "<html lang=\"{}\">".format(lang),
            "<head>"]

        doc_start.append("  <title>{}</title>".format(title))

        if charset:
            doc_start.append("  <meta charset=\"{}\">".format(charset))

        # Include CSS file or import CSS lines into HTML document.
        if css_include:
            doc_start += ["  <link rel=\"stylesheet\" href=\"{}\">".format(css_include)]
        else:
            css_lines = HtmlUtility.GetDefaultCssStyles(border_style)

        if css_lines:
            doc_start += ["  <style>"]
            StringUtility.IndentLines(css_lines, 4)
            doc_start += css_lines
            doc_start += ["  </style>"]

        doc_start += ["</head>", "<body>"]

        return doc_start

    ## Get the document end, as a list of text lines.
    @staticmethod
    def GetDocumentEnd():
        return ["</body>", "</html>"]

    ## Get the default CSS styles.
    @staticmethod
    def GetDefaultCssStyles(boder_style = None):
        if boder_style is None:
            boder_style = ""
        css_lines = [
            "body {",
            "  font-family: sans-serif, serif;",
            "  color: dimgray;",
            "}",
            "h1,h2,h3,h4,h5,h6,h7,h8,h9 {",
            "  border-bottom: 1px solid lightgray;",
            "  width: 100%;",
            "  display: block;",
            "}",
            "hr {",
            "  height: 2px;",
            "  background-color: #ccc;",
            "  border: none;",
            "}",
            "table {",
            "  border-collapse:collapse;",
            "}",
            "table, th, td {",
            "  border: 1px solid dimgrey;",
            "  padding: 5px;",
            "}",
            "th {",
            "  background-color: lightgrey;",
            "}",
            ".multiline {",
            "  white-space: pre-wrap;{}".format(boder_style),
            "}",
            "blockquote {",
            "  white-space:pre-wrap;{}".format(boder_style),
            "}"]

        return css_lines
