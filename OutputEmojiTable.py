#!/usr/bin/env python

# Copyright 2021 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from internal.HtmlCharacterMap import HtmlCharacterMap
from internal.MarkdownEmoji2Html import MarkdownEmoji2Html

if __name__ == "__main__":

    _html_char_map = HtmlCharacterMap()
    _html_emoji_map = MarkdownEmoji2Html()

    _doc_start = [
        "<!DOCTYPE html>",
        "<html lang=\"en\">",
        "<head>",
        "  <style>",
        "  table {",
        "    border-collapse:collapse;",
        "  }",
        "  table, th, td {",
        "    border: 1px solid black;",
        "  }",
        "  </style>",
        "</head>",
        "<body>" ]

    for line in _doc_start:
        print(line)

    print("<table>")
    print("  <tr>")
    print("    <th>Emoji</th>")
    print("    <th>Markdown Emoji</th>")
    print("    <th>HTML Emoji code</th>")
    print("  </tr>")

    _emoji_dict = _html_emoji_map.GetDictionary()
    for item in _emoji_dict.items():
        markdown_emoji  = item[0]
        html_emoji_code = item[1]
        print("  <tr>")
        print("    <th>{}</th>".format(html_emoji_code))
        print("    <th>:{}:</th>".format(markdown_emoji))
        print("    <th>{}</th>".format(_html_char_map.Escape(html_emoji_code)))
        print("  </tr>")

    print("</table>")
    print("</body>")
