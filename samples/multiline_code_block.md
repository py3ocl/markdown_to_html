```
Some multi-line code text
```
Text not in multi-line code block  
```
Yet more multi-line code text
and yet more multi-line code text
```
More text not in multi-line code block  
and more not in multi-line code block  
```
Yet even more multi-line code text
and yet even more multi-line code text
```
Even more text not in multi-line code block  
and even more not in multi-line code block  

---

```
Some multi-line text after a line.
Some more multi-line text after a line.
```

---

Text following a line not in multi-line code block  
Even more text following a line not in multi-line code block  
