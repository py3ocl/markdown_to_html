HEADER 1
==

HEADER 2
--

# HEADING 1
Some text within heading 1.
## HEADING 2
Some text within heading 2.  
### HEADING 3
Some text within heading 3.
Some more text within heading 3.

#### HEADING 4

|column 1|column 2|column 3|column 4|
|--------|:-------|:------:|-------:|
|text 1  |text 2  |text 3  |text 4  |
|text 5  |text 6  |text 7  |text 8  |

##### HEADING 5

This is a line followed by line break  
This is another line.


Example of some normal text
Example of some more text with double space  
Example of escaped text \*\*\~\> that will be converted.  
  
Example of **bold text** in the middle of a line.

Example of *Italic text* in the middle of a line.

Example of ~~trike through~~ in the middle of a line.

Example of smile :smile: emoji in the middle of a line.

Following two blank lines are forced with new line  
  
  
Example of a line of text before blockquote text.
> Some blockquotes text.
>
>> Indented blockquotes text.

Some `code` within a text line

```
Multi-line code block
More code lines
```

Text before horizontal line...

---

Text after horizontal rule.

![](header_image.jpg)

![some alt text](header_image.jpg)

image in the middle ![some alt text](header_image.jpg) of this.

[py3ocl link](https://gitlab.com/py3ocl/)

a link in the middle [py3ocl link](https://gitlab.com/py3ocl/) of this.

[test link](test_link.html)

- Unordered list for first item.
- Unordered list for second item.

1. First item in list.
2. Second item in list.

- [x] First item in a task list.
- [ ] Second item in a task list.

