#!/usr/bin/env python

import sys
sys.path.append("..")

from markdown_to_html.internal.FileUtility import FileUtility

if __name__ == "__main__":
    matched_files = []
    matched_paths = []
    invalid_paths = []
    if FileUtility.FindFiles(matched_files, matched_paths, invalid_paths, ".py", r"C:\Projects\gitlab\py3ocl\markdown_to_html"):
        print("files...")
        for f in matched_files:
            print(f)
        if matched_paths:
            print("paths...")
            for p in matched_paths:
             print(p)
        if invalid_paths:
            print("Invalid paths...")
            for f in invalid_paths:
                print(f)
    else:
        print("error")
